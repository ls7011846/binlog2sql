使用venv
/Users/ls/.venv/bin/activate
source activate
# 查看是否开启Binlog
mysql> show variables like "log_bin";
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_bin       | ON    |
+---------------+-------+
1 row in set (0.02 sec)
# 查看Binlog类型
mysql> show variables like "binlog_format";
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| binlog_format | ROW   |
+---------------+-------+
1 row in set (0.03 sec)

查看binlog
show master logs;

show binlog events in 'mysql-bin.000001';


show binlog events in 'mysql-bin.000001';

purge master logs before '2020-5-18 00:00:00';

show master status;

show master logs;

purge master logs to 'mysql-bin.000001';

解析出sql
python binlog2sql.py -h127.0.0.1 -P3306 -uadmin -p'admin' -dtest -t test3 test4 --start-file='mysql-bin.000002'

解析出回滚sql
python binlog2sql.py --flashback -h127.0.0.1 -P3306 -uadmin -p'admin' -dtest -ttest3 --start-file='mysql-bin.000002' --start-position=763 --stop-position=1147


自测
python binlog2sql/binlog2sql.py -h123.206.77.179 -P3306 -uroot -p'7011846ls' -d mytest -t tb_library_book test4 --start-file='mysql-bin.000002' >>

